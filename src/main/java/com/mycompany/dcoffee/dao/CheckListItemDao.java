/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dcoffee.dao;

import com.mycompany.dcoffee.helper.DatabaseHelper;
import com.mycompany.dcoffee.model.CheckListItem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author PC Sakda
 */
public class CheckListItemDao implements Dao<CheckListItem> {

    @Override
    public CheckListItem get(int id) {
        CheckListItem checklistitem = null;
        String sql = "SELECT * FROM checklist_item WHERE checklist_item_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                checklistitem = checklistitem.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checklistitem;
    }

    @Override
    public List<CheckListItem> getAll() {
        ArrayList<CheckListItem> list = new ArrayList();
        String sql = "SELECT * FROM checklist_item";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckListItem checklistitem = CheckListItem.fromRS(rs);
                list.add(checklistitem);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckListItem save(CheckListItem obj) {
        String sql = "INSERT INTO checklist_item (checklist_item_quantity, ingredient_name, ingredient_quantity ,ingredient_unit,ingredient_min,ingredient_id,checklist_id)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1, obj.getItemquantity());
            stmt.setString(2, obj.getName());
            stmt.setDouble(3, obj.getQuantity());
            stmt.setString(4, obj.getUnit());
            stmt.setInt(5, obj.getMin());
            stmt.setInt(6, obj.getIngredientId());
            stmt.setInt(7, obj.getChecklistId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckListItem update(CheckListItem obj) {
        String sql = "UPDATE checklist_item"
                + " SET checklist_item_quantity = ?, ingredient_name = ?, ingredient_quantity = ?, ingredient_unit = ?, ingredient_min = ?, ingredient_id = ?, checklist_id = ?"
                + " WHERE checklist_item_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1, obj.getItemquantity());
            stmt.setString(2, obj.getName());
            stmt.setDouble(3, obj.getQuantity());
            stmt.setString(4, obj.getUnit());
            stmt.setInt(5, obj.getMin());
            stmt.setInt(6, obj.getIngredientId());
            stmt.setInt(7, obj.getChecklistId());
            stmt.setDouble(8, obj.getId());
            int ret = stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckListItem obj) {
        String sql = "DELETE FROM checklist_item WHERE checklist_item_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<CheckListItem> getAll(String where, String order) {
        ArrayList<CheckListItem> list = new ArrayList();
        String sql = "SELECT * FROM checklist_item where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckListItem checklistitem = CheckListItem.fromRS(rs);
                list.add(checklistitem);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
