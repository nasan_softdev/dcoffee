/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffee.dao;

import com.mycompany.dcoffee.helper.DatabaseHelper;
import com.mycompany.dcoffee.model.Member;
import com.mycompany.dcoffee.model.Product;
import com.mycompany.dcoffee.model.Receipt;
import com.mycompany.dcoffee.model.ReceiptItem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kitty
 */
public class ReceiptDao implements Dao<Receipt> {

    @Override
    public Receipt get(int id) {
        Receipt item = null;
        String sql = "SELECT * FROM  receipt WHERE receipt_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Receipt.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;

    }


    @Override
    public List<Receipt> getAll() {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM  receipt";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt item = Receipt.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }

    @Override
    public Receipt save(Receipt obj) {
        ReceiptItemDao receiptItemDao = new ReceiptItemDao();
        MemberDao memberDao = new MemberDao();
        Member member = memberDao.get(obj.getMemberId());
        member.setPoint((int) (member.getPoint() - obj.getDiscount()));
        String sql = "INSERT INTO receipt (receipt_total,receipt_discount,receipt_cash,receipt_change,receipt_queue_num,receipt_timestamp,employee_id,member_id)"
                + "VALUES(?, ?, ?, ?, ? ,?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1, obj.getTotal());
            stmt.setDouble(2, obj.getDiscount());
            stmt.setDouble(3, obj.getCash());
            stmt.setDouble(4, obj.getChange());
            stmt.setInt(5, obj.getQueue());
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            stmt.setString(6, dateFormat.format(obj.getDate()));
            stmt.setInt(7, obj.getEmployeeId());
            stmt.setInt(8, obj.getMemberId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
            for (ReceiptItem receipt_Item : obj.getReceiptItem()) {
                ReceiptItem receiptItem = receiptItemDao.save(receipt_Item, id);
            }
            return obj;

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }

    }

    @Override
    public Receipt update(Receipt obj) {
        String sql = "UPDATE receipt"
                + " SET receipt_total = ?, receipt_discount = ?, receipt_cash = ?, receipt_change = ?, receipt_queue_num = ?, receipt_timestamp = ?, employee_id = ?, member_id = ?"
                + " WHERE receipt_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            stmt.setDouble(1, obj.getTotal());
            stmt.setDouble(2, obj.getDiscount());
            stmt.setDouble(3, obj.getCash());
            stmt.setDouble(4, obj.getChange());
            stmt.setInt(5, obj.getQueue());
            stmt.setString(6, dateFormat.format(obj.getDate()));
            stmt.setInt(7, obj.getEmployeeId());
            stmt.setInt(8, obj.getMemberId());
            stmt.setInt(9, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Receipt obj) {
        String sql = "DELETE FROM receipt WHERE receipt_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;

    }

    @Override
    public List<Receipt> getAll(String where, String order) {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM receipt where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt item = Receipt.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }

}
