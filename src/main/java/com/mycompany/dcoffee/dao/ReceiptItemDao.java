/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffee.dao;

import com.mycompany.dcoffee.helper.DatabaseHelper;
import com.mycompany.dcoffee.model.Product;
import com.mycompany.dcoffee.model.ReceiptItem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Melon
 */
public class ReceiptItemDao implements Dao<ReceiptItem> {

    @Override
    public ReceiptItem get(int id) {
        ReceiptItem item = null;
        String sql = "SELECT * FROM receipt_item WHERE receipt_item_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = ReceiptItem.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<ReceiptItem> getAll() {
        ArrayList<ReceiptItem> list = new ArrayList();
        String sql = "SELECT * FROM receipt_item";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptItem item = ReceiptItem.fromRS(rs);
                list.add(item);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public ReceiptItem save(ReceiptItem obj) {
        String sql = "INSERT INTO receipt_item (receipt_item_amount, receipt_item_price, receipt_item_total, receipt_id, product_id)" + "VALUES(?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getAmount());
            stmt.setDouble(2, obj.getPrice());
            stmt.setDouble(3, obj.getTotal());
            stmt.setInt(4, obj.getReceiptId());
            stmt.setInt(5, obj.getProductId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;

    }

    public ReceiptItem save(ReceiptItem obj, int id) {
        String sql = "INSERT INTO receipt_item (receipt_item_amount, receipt_item_price, receipt_item_total, receipt_id, product_id)" + "VALUES(?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getAmount());
            stmt.setDouble(2, obj.getPrice());
            stmt.setDouble(3, obj.getTotal());
            stmt.setInt(4, id);
            stmt.setInt(5, obj.getProductId());
            stmt.executeUpdate();
            id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;

    }

//    @Override
//    public ReceiptItem update(ReceiptItem obj) {
//        String sql = "UPDATE receipt_item"
//                + " SET receipt_item_amount = ?, receipt_item_price = ?, receipt_item_total = ?, receipt_id = ?, product_id = ?"
//                + " WHERE receipt_item_id = ?";
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setInt(1, obj.getAmount());
//            stmt.setDouble(2, obj.getPrice());
//            stmt.setDouble(3, obj.getTotal());
//            stmt.setInt(4, obj.getReceiptId());
//            stmt.setInt(5, obj.getProductId());
//            stmt.setInt(6, obj.getId());
//            int ret = stmt.executeUpdate();
////            System.out.println(ret);
//            return obj;
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//            return null;
//        }
//    }
    
    public ReceiptItem update(ReceiptItem obj, int id) {
        String sql = "UPDATE receipt_item"
                + " SET receipt_item_amount = ?, receipt_item_price = ?, receipt_item_total = ?, receipt_id = ?, product_id = ?"
                + " WHERE receipt_item_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getAmount());
            stmt.setDouble(2, obj.getPrice());
            stmt.setDouble(3, obj.getTotal());
            stmt.setInt(4, obj.getReceiptId());
            stmt.setInt(5, id);
            stmt.setInt(6, obj.getId());
            int ret = stmt.executeUpdate();
//            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(ReceiptItem obj) {
        String sql = "DELETE FROM receipt_item WHERE receipt_item_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;

    }

    @Override
    public List<ReceiptItem> getAll(String where, String order) {
        ArrayList<ReceiptItem> list = new ArrayList();
        String sql = "SELECT * FROM receipt_item where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptItem item = ReceiptItem.fromRS(rs);
                list.add(item);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }

    //unuse update
    @Override
    public ReceiptItem update(ReceiptItem obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
