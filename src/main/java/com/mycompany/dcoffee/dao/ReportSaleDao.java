/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffee.dao;

import com.mycompany.dcoffee.helper.DatabaseHelper;
import com.mycompany.dcoffee.model.ReportSale;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nasan
 */
public class ReportSaleDao {

    public List<ReportSale> getYearReport() {
        ArrayList<ReportSale> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y\", receipt_timestamp) as period,SUM(receipt_total) as total FROM receipt GROUP BY strftime(\"%Y\", receipt_timestamp)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSale item = ReportSale.fromRS(rs);
                list.add(item);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReportSale> getMonthReport() {
        ArrayList<ReportSale> list = new ArrayList();
        String sql = "SELECT strftime(\"%m\", receipt_timestamp) as period,SUM(receipt_total) as total FROM receipt";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                ReportSale item = ReportSale.fromRS(rs);
                list.add(item);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<ReportSale> getSelectedYearReport(String selectedYear) {

        ArrayList<ReportSale> list = new ArrayList();
        String sql = "SELECT strftime(\"%m\", receipt_timestamp) as period,SUM(receipt_total) as total FROM receipt WHERE strftime(\"%Y\", receipt_timestamp) = " + "\"" + selectedYear + "\" " + "GROUP BY strftime(\"%m\", receipt_timestamp) ORDER BY total ASC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSale item = ReportSale.fromRS(rs);
                list.add(item);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
     public List<ReportSale> getPayrollMonthReport() {
        ArrayList<ReportSale> list = new ArrayList();
        String sql = "SELECT strftime(\"%m\", check_time_in) as period"
                + "FROM check_time\n"
                + "GROUP BY period\n"
                + "ORDER BY period DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSale item = ReportSale.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<ReportSale> getSelectedNameReport(String selectedName) {

        ArrayList<ReportSale> list = new ArrayList();
        String sql = "SELECT employee_name FROM employee WHERE * = " + "\"" + selectedName + "\" ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSale item = ReportSale.fromRS(rs);
                list.add(item);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
