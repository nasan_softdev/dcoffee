/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dcoffee.model;

import java.util.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class Bill {

    private int id;
    private Date date;
    private int employeeId;

    public Bill(int id, Date date, int employeeId) {
        this.id = id;
        this.date = date;
        this.employeeId = employeeId;
    }

    public Bill(Date date, int employeeId) {
        this.id = -1;
        this.date = date;
        this.employeeId = employeeId;
    }

    public Bill() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public String toString() {
        return "Bill{" + "id=" + id + ", date=" + date + ", employeeId=" + employeeId + '}';
    }

    public static Bill fromRS(ResultSet rs) {
        Bill bill = new Bill();
        try {
            bill.setId(rs.getInt("bill_id"));
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String date = rs.getString("bill_date");
            try {
                bill.setDate(df.parse(date));
            } catch (ParseException ex) {
                Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
            }
            bill.setEmployeeId(rs.getInt("employee_id"));
        } catch (SQLException ex) {
            Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return bill;
    }
}
