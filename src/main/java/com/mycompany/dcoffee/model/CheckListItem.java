/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PC Sakda
 */
public class CheckListItem {

    private int id;
    private double itemquantity;
    private String name;
    private double quantity;
    private String unit;
    private int min;
    private int ingredientId;
    private int checklistId;

    public CheckListItem(int id, double itemquantity, String name, double quantity, String unit, int min, int ingredientId, int checklistId) {
        this.id = id;
        this.itemquantity = itemquantity;
        this.name = name;
        this.quantity = quantity;
        this.unit = unit;
        this.min = min;
        this.ingredientId = ingredientId;
        this.checklistId = checklistId;
    }

    public CheckListItem(double itemquantity, String name, double quantity, String unit, int min, int ingredientId, int checklistId) {
        this.id = -1;
        this.itemquantity = itemquantity;
        this.name = name;
        this.quantity = quantity;
        this.unit = unit;
        this.min = min;
        this.ingredientId = ingredientId;
        this.checklistId = checklistId;
    }

    public CheckListItem() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getItemquantity() {
        return itemquantity;
    }

    public void setItemquantity(double itemquantity) {
        this.itemquantity = itemquantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(int ingredientId) {
        this.ingredientId = ingredientId;
    }

    public int getChecklistId() {
        return checklistId;
    }

    public void setChecklistId(int checklistId) {
        this.checklistId = checklistId;
    }

    @Override
    public String toString() {
        return "CheckListItem{" + "id=" + id + ", itemquantity=" + itemquantity + ", name=" + name + ", quantity=" + quantity + ", unit=" + unit + ", min=" + min + ", ingredientId=" + ingredientId + ", checklistId=" + checklistId + '}';
    }

    public static CheckListItem fromRS(ResultSet rs) {
        CheckListItem checklistitem = new CheckListItem();
        try {
            checklistitem.setId(rs.getInt("checklist_item_id"));
            checklistitem.setItemquantity(rs.getDouble("checklist_item_quantity"));
            checklistitem.setName(rs.getString("ingredient_name"));
            checklistitem.setQuantity(rs.getDouble("ingredient_quantity"));
            checklistitem.setUnit(rs.getString("ingredient_unit"));
            checklistitem.setMin(rs.getInt("ingredient_min"));
            checklistitem.setIngredientId(rs.getInt("ingredient_id"));
            checklistitem.setChecklistId(rs.getInt("checklist_id"));
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checklistitem;
    }

}
