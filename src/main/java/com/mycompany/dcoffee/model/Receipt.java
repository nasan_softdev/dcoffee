/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kitty
 */
public class Receipt {

    private int id;
    private double total;
    private double discount;
    private double cash;
    private double change;
    private int queue;
    private Date timestamp;
    private int employeeId;
    private int memberId;
    private ArrayList<ReceiptItem> receiptItem = new ArrayList<>();

    public Receipt(int id, double total, double discount, double cash, double change, int queue, Date timestamp, int employeeId, int memberId) {
        this.id = id;
        this.discount = discount;
        this.total = total;
        this.cash = cash;
        this.change = change;
        this.queue = queue;
        this.timestamp = timestamp;
        this.employeeId = employeeId;
        this.memberId = memberId;
    }

    public Receipt(double total, double discount, double cash, double change, int queue, Date timestamp, int employeeId, int memberId) {
        this.id = -1;
        this.discount = discount;
        this.total = total;
        this.cash = cash;
        this.change = change;
        this.queue = queue;
        this.timestamp = timestamp;
        this.employeeId = employeeId;
        this.memberId = memberId;
    }

    public Receipt(Date timestamp) {
        this.timestamp = timestamp;

    }

    public Receipt() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getCash() {
        return cash;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

    public double getChange() {
        return change;
    }

    public void setChange(double change) {
        this.change = change;
    }

    public int getQueue() {
        return queue;
    }

    public void setQueue(int queue) {
        this.queue = queue;
    }

    public Date getDate() {
        return timestamp;
    }

    public void setDate(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public ArrayList<ReceiptItem> getReceiptItem() {
        return receiptItem;
    }

    public void setReceiptItem(ArrayList<ReceiptItem> receiptItem) {
        this.receiptItem = receiptItem;
    }

    public int getArraySize() {
        return receiptItem.size();
    }

    public void addReciept(Product product, int quantity) {
        ReceiptItem receipt_Item = new ReceiptItem(quantity, product.getPrice(), (int) (product.getPrice() * quantity), this.getId(), product.getId());
        receiptItem.add(receipt_Item);
    }



    @Override
    public String toString() {
        return "Receipt{" + "id=" + id + ", total=" + total + ", discount=" + discount + ", cash=" + cash + ", change=" + change + ", queue=" + queue + ", timestamp=" + timestamp + ", employeeId=" + employeeId + ", memberId=" + memberId + '}';
    }

    public static Receipt fromRS(ResultSet rs) {
        Receipt receipt = new Receipt();
        try {
            receipt.setId(rs.getInt("receipt_id"));
            receipt.setTotal(rs.getDouble("receipt_total"));
            receipt.setDiscount(rs.getDouble("receipt_discount"));
            receipt.setCash(rs.getDouble("receipt_cash"));
            receipt.setChange(rs.getDouble("receipt_change"));
            receipt.setQueue(rs.getInt("receipt_queue_num"));
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String date = rs.getString("receipt_timestamp");
            try {
                receipt.setDate(df.parse(date));
            } catch (ParseException ex) {
                Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
            }
            receipt.setEmployeeId(rs.getInt("employee_id"));
            receipt.setMemberId(rs.getInt("member_id"));
        } catch (SQLException ex) {
            Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return receipt;

    }

}
