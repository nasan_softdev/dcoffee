/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Melon
 */
public class ReceiptItem {

    private int id;
    private int amount;
    private double price;
    private double total;
    private int receiptId;
    private int productId;

    public ReceiptItem(int id, int amount, double price, double total, int receiptId, int productId) {
        this.id = id;
        this.amount = amount;
        this.price = price;
        this.total = total;
        this.receiptId = receiptId;
        this.productId = productId;
    }

    public ReceiptItem(int amount, double price, double total, int receiptId, int productId) {
        this.id = -1;
        this.amount = amount;
        this.price = price;
        this.total = total;
        this.receiptId = receiptId;
        this.productId = productId;
    }

    public ReceiptItem() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public int getReceiptId() {
        return receiptId;
    }

    public void setReceiptId(int receiptId) {
        this.receiptId = receiptId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public static ReceiptItem fromRS(ResultSet rs) {
        ReceiptItem receiptItem = new ReceiptItem();
        try {
            receiptItem.setId(rs.getInt("receipt_item_id"));
            receiptItem.setAmount(rs.getInt("receipt_item_amount"));
            receiptItem.setPrice(rs.getDouble("receipt_item_price"));
            receiptItem.setTotal(rs.getDouble("receipt_item_total"));
            receiptItem.setReceiptId(rs.getInt("receipt_id"));
            receiptItem.setProductId(rs.getInt("product_id"));
        } catch (SQLException ex) {
            Logger.getLogger(ReceiptItem.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return receiptItem;
    }

    @Override
    public String toString() {
        return "ReceiptItem{" + "id=" + id + ", amount=" + amount + ", price=" + price + ", total=" + total + ", receiptId=" + receiptId + ", productId=" + productId + '}';
    }


}
