/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pinkz7_
 */
public class ReportPayroll {
    String period;

    public ReportPayroll() {
    }

    public ReportPayroll(String period) {
        this.period = period;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    @Override
    public String toString() {
        return "ReportPayroll{" + "period=" + period + '}';
    }
    
    public static ReportPayroll fromRS(ResultSet rs) {
        ReportPayroll obj = new ReportPayroll();
        try {
            obj.setPeriod(rs.getString("period"));
            return obj;
        } catch (SQLException ex) {
            Logger.getLogger(ReportPayroll.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }
}
