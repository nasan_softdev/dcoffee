/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffee.poc;

import com.mycompany.dcoffee.model.CheckTime;
import com.mycompany.dcoffee.dao.CheckTimeDao;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Pinkz7_
 */
public class TestCheckTimeDao {
    public static void main(String[] args) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String dateFormatIn = "2022-10-31 01:00";
        String dateFormatOut = "2022-10-3  20:50";
        Date in = df.parse(dateFormatIn);
        Date out = df.parse(dateFormatOut);
        CheckTime checktime = new CheckTime(in,out,9,"n",1,2);
        CheckTimeDao checktimeDao = new CheckTimeDao();
        
        // Test save
//        checktimeDao.save(checktime);
        
//        // Test get
//        CheckTime updateCheckTime = checktimeDao.get(8);
//        System.out.println(updateCheckTime);
//        
//        // Test update
//        updateCheckTime.setPayment("n");
//        checktimeDao.update(updateCheckTime);
        
        // Test delete
        checktimeDao.delete(checktimeDao.get(8));
    
    }
}
