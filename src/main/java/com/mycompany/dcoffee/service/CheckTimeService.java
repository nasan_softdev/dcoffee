/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffee.service;

import com.mycompany.dcoffee.dao.CheckTimeDao;
import com.mycompany.dcoffee.model.CheckTime;
import com.mycompany.dcoffee.model.User;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author nasan
 */
public class CheckTimeService {

    public static List<CheckTime> getCheckTime() {
        CheckTimeDao checkTimeDao = new CheckTimeDao();
        return checkTimeDao.getAll();
    }

    public static String formatedDate(Date date) {
//        Date date = checkTime.getIn();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String formatedDate = df.format(date);
        return formatedDate;
    }

    public static void login(User user) throws ParseException {
        CheckTimeDao checkTimeDao = new CheckTimeDao();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd ");
        Date date = new Date();
        Date dateOut = df.parse(df.format(date));

//        System.out.println(df.format(date));
        CheckTime checkTime = new CheckTime(date, dateOut, 0, "n", user.getEmployeeId(), 0);
        checkTimeDao.save(checkTime);
    }

    public static void logout(int time) throws ParseException {

    }

    public static void changePayStatus(CheckTime checktime, int payrollId) {
        CheckTimeDao checkTimeDao = new CheckTimeDao();
        checktime.setPayment("y");
        checktime.setPayrollId(payrollId);
        checkTimeDao.update(checktime);
    }

    public List<CheckTime> getReportByYear(String year, int index) {
        CheckTimeDao payrollReportDao = new CheckTimeDao();
        return CheckTimeDao.getYearReport(year, index);
    }

    public List<CheckTime> getReportByMonthYear(String year, int index, String month) {
        CheckTimeDao payrollReportDao = new CheckTimeDao();
        return CheckTimeDao.getMonthYearReport(year, index, month);
    }

}
