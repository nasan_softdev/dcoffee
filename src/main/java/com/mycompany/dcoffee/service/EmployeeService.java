/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffee.service;

import com.mycompany.dcoffee.dao.EmployeeDao;
import com.mycompany.dcoffee.model.Employee;
import java.util.List;

/**
 *
 * @author nasan
 */
public class EmployeeService {

    public  String getName(int id) {
        EmployeeDao employeeDao = new EmployeeDao();
        Employee employee = employeeDao.get(id);

        return employee.getName();
    }
   
    public String[] getAllName() {

        EmployeeDao employeeDao = new EmployeeDao();
        List<Employee> employee = employeeDao.getAll();

        String[] name = new String[employee.size()];

        for (int i = 0; i < employee.size(); i++) {
            name[i] = employee.get(i).getName();
        }

        return name;
    }
}
