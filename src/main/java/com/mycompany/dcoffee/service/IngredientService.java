/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dcoffee.service;

import com.mycompany.dcoffee.dao.IngredientDao;
import com.mycompany.dcoffee.model.Ingredient;
import java.util.List;

/**
 *
 * @author Acer
 */
public class IngredientService {
    
    
    public  List<Ingredient> getIngredient(){
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.getAll();
    }

    public Ingredient addNew(Ingredient editedIngredient) {
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.save(editedIngredient);
    }

    public Ingredient update(Ingredient editedIngredient) {
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.update(editedIngredient);
    }

    public int delete(Ingredient editedIngredient) {
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.delete(editedIngredient);
    }
    
     
}
