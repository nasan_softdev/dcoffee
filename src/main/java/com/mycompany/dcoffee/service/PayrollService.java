/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffee.service;

import com.mycompany.dcoffee.dao.PayrollDao;
import com.mycompany.dcoffee.dao.PayrollReportDao;
import com.mycompany.dcoffee.model.Payroll;
import com.mycompany.dcoffee.model.ReportPayroll;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Pinkz7_
 */
public class PayrollService {

    public static Payroll paySalary(Date date, double salary, int id) {
        PayrollDao payrollDao = new PayrollDao();
        Payroll payroll = new Payroll(date, salary, id);
        return payrollDao.save(payroll);
    }

    public List<Payroll> getPayrolls() {
        PayrollDao payrollDao = new PayrollDao();
        return payrollDao.getAll();
    }

    public List<ReportPayroll> getReporteByDay() {
        PayrollReportDao payrollReportDao = new PayrollReportDao();
        return payrollReportDao.getDayReport();
    }

}
