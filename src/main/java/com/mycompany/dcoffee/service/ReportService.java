/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffee.service;

import com.mycompany.dcoffee.dao.ReportSaleDao;
import com.mycompany.dcoffee.model.ReportSale;
import java.util.List;

/**
 *
 * @author nasan
 */
public class ReportService {

    public List<ReportSale> getReportYear() {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getYearReport();
    }

    public List<ReportSale> getReportMonths() {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getMonthReport();
    }

    public List<ReportSale> getReportSaleBySelectedYear(String year) {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getSelectedYearReport(year);
    }

    public List<ReportSale> getReportPayrollMonths() {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getMonthReport();
    }
    
    public List<ReportSale> getReporPayrollBySelectedName(String name) {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getSelectedYearReport(name);
    }
}
