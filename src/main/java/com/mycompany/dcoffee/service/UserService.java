/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffee.service;

import com.mycompany.dcoffee.dao.UserDao;
import com.mycompany.dcoffee.model.User;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author nasan
 */
public class UserService {

    public static User login(String login, String password) {
        UserDao userDao = new UserDao();
        User user = userDao.getByLogin(login);
        if (user != null && user.getUserpassword().equals(password)) {
            return user;
        }
        return null;
    }

    public static String getUser(String name, String password) {
        UserDao userDao = new UserDao();
        User user = userDao.get(name);
        return user.getUsername();
    }

    public User addNew(User editedUser) {
        UserDao userDao = new UserDao();
        return userDao.save(editedUser);
    }

    public User update(User editedUser) {
        UserDao userDao = new UserDao();
        return userDao.update(editedUser);
    }

    public int delete(User editedUser) {
        UserDao userDao = new UserDao();
        return userDao.delete(editedUser);
    }

    public List<User> getUsers() {
        UserDao userDao = new UserDao();
        return userDao.getAll();
    }

}
