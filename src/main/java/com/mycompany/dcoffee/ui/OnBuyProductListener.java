/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dcoffee.ui;

import com.mycompany.dcoffee.model.Product;

/**
 *
 * @author PC Sakda
 */
public interface OnBuyProductListener {
    public void buy(Product product, int amount);
}